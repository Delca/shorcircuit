
all:
	clear
	g++ src/main.cc src/*/*.cc -o shorCircuit -std=c++0x -Ofast `sdl2-config --cflags --libs` -g -ggdb3

clean:
	rm -rf shorCircuit
	rm -rf js/states.json
	rm -rf *~
	rm -rf */*~
	rm -rf */*/*~

	var widthBars = 600,
	heightBars = 500;
var totalTransTime = 0.5 * 1000;
var fpsBars = 60;

var barsCanvas = d3.select("#chartCanvas")
				   .append("canvas")
				   .attr("width", widthBars)
				   .attr("height", heightBars)
				   .attr("style", "border: 2px solid #110011;");

var contextBars = barsCanvas.node().getContext("2d");
				   
function drawBar(x, y, w, h, color) {
	contextBars.fillStyle = color;
	contextBars.fillRect(x, heightBars-(y+h), w, Math.max(h, 2));
}

function drawNamedBar(x, y, w, h, num, color, numColor) {
	numColor = numColor || 'red';
	drawBar(x, y, w, h, color);
	var fontSize = 30;
	contextBars.font = fontSize + 'px Georgia';
	contextBars.fillStyle = 'red';
	x += Math.max((w-contextBars.measureText(num).width)/2, 0);
	contextBars.fillText(num, x, heightBars - (y + fontSize/2), w);
	contextBars.save();
	contextBars.rotate(-Math.PI/2);
	contextBars.font = (fontSize/2) + 'px Georgia';
	contextBars.fillText(num.toString(2), -heightBars + (y + fontSize/2), x);
	contextBars.restore();
}

var drawFunction;
var updateForSelection = false;
var chartStartIndex = 0;

function drawGraphic() {
	if (!updateForSelection && Date.now() - startTime > totalTransTime) {
		clearInterval(drawFunction);
		drawFunction = undefined;
		// console.log('Animation end');
		return;
	}
	
	var currPerc = (Date.now() - startTime)/totalTransTime;
	currPerc = easing(currPerc);
	currPerc = clamp(0, currPerc, 1);
	var oppCurrPerc = 1 - currPerc;
	
	if (updateForSelection && typeof(drawFunction) == 'undefined') {
		currPerc = 1;
		oppCurrPerc = 0;
	}
	
	var offSetX = 20;
	var offSetY = 20;
	var w = 60;
	var maxBarHeight = 240;
	var spacing = 20;
	var numberOfBar = 7;
	var margin = widthBars - ((numberOfBar - 1)*(w + spacing) + w);
	margin = Math.max(margin / 2, 0);
	offSetX = margin;
	numberOfBar = Math.min(numberOfBar, tab.size);
	contextBars.clearRect(0, 0, widthBars, heightBars);
	
	if (updateForSelection) {
		var alreadyDisplayed = false;
		var targetNode = ((selectionConfirmed && typeof(cursorNode) != 'undefined')?cursorNode:selectedNode);
		if (targetNode)	targetNode = nodes[targetNode].key;
		for (var i = chartStartIndex; i < numberOfBar; ++i) {
			if (tab.keys[i] == targetNode) {
				alreadyDisplayed = true;
			}
		}
		if (!alreadyDisplayed) {
			console.log('Looking for new starting index');
			for (var i = 0; i < tab.keys.length; ++i) {
			if (tab.keys[i] == targetNode) {
				chartStartIndex = clamp(0, i - Math.ceil(numberOfBar/2), Math.max(tab.keys.length - numberOfBar, 0));
				console.log('Starting at ' + chartStartIndex);
				
				break;
				}
			}
		}

	}
		
	for (var i = chartStartIndex; i < chartStartIndex+numberOfBar; ++i) {
		var ind = tab.keys[i];
	
		var currBarSize = (tab[ind]?tab[ind].amp * maxBarHeight:0);
		var oldBarSize = (oldTab[ind]?oldTab[ind].amp * maxBarHeight:0);
		var h = Math.floor(currPerc*currBarSize + oppCurrPerc*oldBarSize);
		
		if (cursorNode && nodes[cursorNode].key == ind) {
			var selectedMargin = 3;
			drawBar(offSetX + (w+spacing)*(i-chartStartIndex)-selectedMargin, offSetY-selectedMargin, w + 2*selectedMargin, h + 2*selectedMargin, "green");
		}
		
		if (selectedNode && nodes[selectedNode].key == ind) {
			var selectedMargin = 3;
			drawBar(offSetX + (w+spacing)*(i-chartStartIndex)-selectedMargin, offSetY-selectedMargin, w + 2*selectedMargin, h + 2*selectedMargin, "red");
		}
		
		drawNamedBar(offSetX + (w+spacing)*(i-chartStartIndex), offSetY, w, h, ind, "lightblue");
		
		
		
	}
	
	updateForSelection = false;
}

function updateBarsChart () {
	startTime = Date.now();
	drawFunction = setInterval(drawGraphic, 1000/fpsBars);
}

function updateBarsChartWithSelection() {
	updateForSelection = true;
	drawGraphic();
}

// var tab = [];

// for (var i = 0; i < 7; ++i) {
	// tab[i] = {
		// oldValue:50*i+20,
		// value:0,
		// key:i*32};
// }



var widthBubbles = 400,
    heightBubbles = 500;

var bowlWidth = 350;
var defaultRadius = 3;
	
function clamp(a, b, c) {
	return Math.min(c, Math.max(a, b));
}

var nodeNum = 1024;
var speedFactor = 0.07;

var force = d3.layout.force()
    .gravity(0.02)
	.friction(0.0001)
    .charge(function(d, i) { return i ? 0 : 0; })
    .size([widthBubbles, 0])
	.alpha(0);
	
force.on('end', function() {
	placingNodes = false;
});

force.on('start', function() {
	placingNodes = true;
});
	
var nodes = force.nodes();
    // rootUp = {type:0, radius: 16, x:(widthBubbles / 2), y:-(heightBubbles / 2), fixed: true};
	console.log(rootUp);
    var rootUp = {type:0, radius: 10, oldRadius: 10, x:(widthBubbles / 2), y:(4*heightBubbles / 8), fixed: true};
	var rootDown = {type:0.1, radius: 10, oldRadius: 10, x:(widthBubbles / 2), y:(10*heightBubbles / 8), fixed: true};
	nodes.push(rootUp);
	nodes.push(rootDown);	


// Activated and deactivated nodes
var aNodes = [];
var dNodes = [];

// Mouse object
var myCursor = {x: 0, y: 0};

// Selected index
var cursorNode = undefined;
var cursorNodeInd;
var selectedNode = undefined;
var selectedNodeInd;
var selectionConfirmed = false;

//this.watch('selectedNode', function(i, o, n) {debugger;});
	
for(var i = 0; i < nodeNum; ++i) {
	var newNode = {
		type:1,
		x:Math.random()*widthBubbles,
		y:(2*heightBubbles)-50,
		radius:defaultRadius, //Math.random()*3 + 4
		fixed:true
		};

	nodes.push(newNode);
	dNodes.push(newNode);
}

var canvas = d3.select("#bubblesCanvas").append("canvas")
    .attr("width", widthBubbles)
    .attr("height", heightBubbles)
	.attr("style", "border: 2px solid #110011;");

/*
var button = d3.select("body").append("button")
	.html("REVERSE");

	
button.on("click", function() {
	console.log("REVERSE");
	force.resume();
	for(var i = 2; i <= nodes.length-1; ++i) {
		nodes[i].type *= -1;
	}
});
*/
var context = canvas.node().getContext("2d");

function drawSingleNode(d, nodeInd, selector) {
	context.moveTo(d.x, d.y);
	context.beginPath();
	if (d.type == 0) {context.fillStyle = "yellow"; context.moveTo(d.x, d.y);}
	else if (d.type == 0.1) context.fillStyle = "green";
	else if (d.type == 1) context.fillStyle = "steelblue";
	else context.fillStyle = "rebeccapurple";
		
	if (typeof(drawFunction) !== 'undefined') {
		var currPerc = (Date.now() - startTime)/totalTransTime;
		currPerc = easing(currPerc);
		currPerc = clamp(0, currPerc, 1);
		var oppCurrPerc = 1 - currPerc;
		d.currRadius = currPerc*d.radius + oppCurrPerc*(d.oldRadius || 0);
	}
	else {
		d.currRadius = d.radius;
	}
	
    	//context.arc(d.x, d.y, d.currRadius, 0, 2 * Math.PI);
	context.rect(d.x, d.y, 2, 2);	
	context.fill();
	
	context.beginPath();
	context.style = "black";
	//context.arc(d.x, d.y, d.currRadius, 0, 2 * Math.PI);
	context.stroke();
	
	if (selector) {
		if (Math.pow(myCursor.x - d.x, 2) + Math.pow(myCursor.y - d.y, 2) < d.currRadius*d.currRadius) {
		cursorNode = nodeInd;
		updateBarsChartWithSelection();
		}
	
	if (typeof(selectedNode) == 'undefined') {
		selectedNode = cursorNode;
		if (typeof(selectedNode) != 'undefined') {
			updateBarsChartWithSelection();
		}
	}
	}
}

function displayNodes() {
	var i, d, n = nodes.length;	
	
  if (!selectionConfirmed) {
	selectedNode = undefined;
  }
  cursorNode = undefined;
  
  context.fillStyle = "lightblue";
  context.fillRect(0, 0, widthBubbles, heightBubbles);
  context.fillStyle = "steelblue";
  
  for (i = 2; i < n; ++i) {
    d = nodes[i];
	
    drawSingleNode(d, i, true);
	
	// Select circle	
	
  }
	
	if (typeof(selectedNode) != 'undefined') {
		var d = nodes[selectedNode];
		drawSingleNode(d, i, false);
		context.beginPath();
		context.fillStyle = "red";
		context.arc(d.x, d.y, d.currRadius/2, 0, 2 * Math.PI);
		context.fill();
	}
	
	if (typeof(selectedNode) != 'undefined' && typeof(cursorNode) != 'undefined' && cursorNode != selectedNode && selectionConfirmed) {
		var d = nodes[cursorNode];
		drawSingleNode(d, i, false);
		context.beginPath();
		context.fillStyle = "green";
		context.arc(d.x, d.y, 0.9*(d.currRadius/2), 0, 2 * Math.PI);
		context.fill();
	}
	
	updateText();
}

function getSquaredDistRadius(node)  {
	return -1//Math.pow(Math.floor(node.radius/2), 2)*128;
}

force.on("tick", function(e) {
  var aTree = d3.geom.quadtree(aNodes),
	  dTree = d3.geom.quadtree(dNodes),
      i,
      d,
      n = nodes.length;
	  
  for (i = 2; i < n; ++i){
	// Target a specific node
	node = nodes[i];
	target = (nodes[i].type == -1?rootUp:rootDown);
	// target = rootDown;
	
	// nodes[i].x += ((target.x - node.x)<0?1:-1) * speedFactor * 2;
	
	if (i == 2) {
		// console.log('T ' + target.x + ' ' + target.y);
		// console.log('N ' + nodes[i].x + ' ' + nodes[i].y);
		// console.log('D ' + (target.y - node.y) );
	}
	
	//dist
	if (nodes[i].type == 1
		|| (target.x - node.x)*(target.x - node.x) + (target.y - node.y)*(target.y - node.y) >= getSquaredDistRadius(node))
		{
			var targetX = target.x;
			var targetY = target.y;

			if (typeof(node.key) != 'undefined') {
				targetX += 100 * Math.sign(tab[node.key].real);
				targetY += 100 * Math.sign(tab[node.key].imag);		
			}

			nodes[i].x += ( (targetX - node.x) * speedFactor);
			nodes[i].y += ( (targetY - node.y) * speedFactor);
			
			nodes[i].px = nodes[i].x;
			nodes[i].py = nodes[i].y;
		}

	
	//console.log(Math.sign(target.y - node.y));
	
	if (i == 2) {
		// console.log('N ' + nodes[i].x + ' ' + nodes[i].y);
	}
	
	// Collide with other nodes
	if (node.type == -1) {
		aTree.visit(collide(nodes[i]));
	}
	
	// if (node.type == 1) {
		// dTree.visit(collide(nodes[i]));
	// }
	
	// Keep contained in the bowl
	var margin = nodes[i].radius;
	nodes[i].x = clamp(margin, nodes[i].x, widthBubbles - margin);
	nodes[i].y = clamp(margin, nodes[i].y, 2*heightBubbles - margin);
  }

  displayNodes();
  
   //force.alpha(1);
});

var placingNodes = true;

canvas.on("mousemove", function() {
	var p1 = d3.mouse(this);
	myCursor.x = p1[0];
	myCursor.y = p1[1];
	if (!placingNodes) {
		displayNodes();
		placingNodes = false;
	}
});

canvas.on("click", function() {

if (selectionConfirmed) {
	if (typeof(cursorNode) == 'undefined') {
		selectionConfirmed = false;
		selectedNode = undefined;
		updateBarsChartWithSelection();
	}
	else {
		selectedNode = cursorNode;
		updateBarsChartWithSelection();
	}
}
else {
	if (typeof(selectedNode) !== 'undefined') {
		selectionConfirmed = true;
		updateBarsChartWithSelection();
	}
}

});

function collide(node) {
  var r = node.radius,
      nx1 = node.x - r,
      nx2 = node.x + r,
      ny1 = node.y - r,
      ny2 = node.y + r;
  return function(quad, x1, y1, x2, y2) {
    if (quad.point && (quad.point !== node)) {
      var x = node.x - quad.point.x,
          y = node.y - quad.point.y,
          l = Math.sqrt(x * x + y * y),
          r = node.radius + quad.point.radius;
      if (l < r) {
        l = (l - r) / l * .5;
        node.x -= x *= l;
        node.y -= y *= l;
        quad.point.x += x;
        quad.point.y += y;
      }
    }
    return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
  };
}

function updateBubbles() {
	var nodesByKey = {};
	var freeNodes = [];

	aNodes = [];
	dNodes = [];
	
	for (var i = nodes.length-1; i >= 2; --i) {
		nodes[i].oldRadius = nodes[i].radius;
		nodes[i].radius = defaultRadius;
		nodes[i].type = 1;
		if (typeof(nodes[i].key) !== 'undefined') {
			nodesByKey[nodes[i].key] = i;
		}
		else {
			freeNodes.push(i);
		}
		nodes[i].key = undefined;
	}
	

	
	var radiusMax = 16 * 1.5;
	
	var links = [];
	var linkStrength = [];
	
	for (var i = 0; i < tab.size; ++i) {
		var key = tab.keys[i];
		
		var nodeInd;
		if (typeof(nodesByKey[key]) !== 'undefined') {
			nodeInd = nodesByKey[key];
		}
		else if (freeNodes.length > 0) {
			nodeInd = freeNodes.pop();
		}
		else {
			break;
		}
		
		nodes[nodeInd].key = key;
		nodes[nodeInd].radius = Math.max(tab[key].amp * radiusMax, 8);
		nodes[nodeInd].type = -1;
	}
	
	for(var i = 2; i <= nodes.length-1; ++i) {
		if (typeof(nodes[i].key) !== 'undefined') {
			aNodes.push(nodes[i]);
		}
		else {
			dNodes.push(nodes[i]);
		}
	}
	
	// for(var i = 0; i < aNodes.length; ++i) {
		// for(var j = i+1; j < aNodes.length; ++j) {
			// links.push({source:nodes[i], target:nodes[j]});
			// linkStrength.push(nodes[i].radius + nodes[j].radius);
		// }
	// }
	
	/*for(var i = 0; i < aNodes.length; ++i) {
		links.push({source:rootUp, target:nodes[i]});
		linkStrength.push(2 * nodes[i].radius);
	}
	
	force.links(links).linkStrength(linkStrength);*/
	
	force.resume();
}

window.onload = function () {
	force.start();
}








//var states = [];
var currStateIndex = -1;

/*
for(var i = 0; i < 15; ++i) {

states[i] = {
	title:'Etape #'+i,
	keys:[]
};

var maxState = 4096;
var size = 0;
for(var j = 0; j < maxState; ++j) {
	
	if (Math.random() > 0.8) continue;
	
	states[i][j] = {
		key: j,
		amp: Math.random()	
	};
	states[i].keys.push(j);
	++size;
}
states[i].size = size;

}
*/

function cubicEasing(x) {
	return x*x*x;
}
function sinusEasing(x) {
	return Math.sin(x*Math.PI/2);
}

var startTime = Date.now();
var easing = sinusEasing;

var currInd = currStateIndex;
var prevInd = Math.max(currStateIndex-1, 0);
var tab = states[0];
var oldTab = states[0];



function changeState(newInd) {
	if (drawFunction === undefined) {
		prevInd = Math.max(currInd, 0);
		oldTab = states[prevInd];
		currInd = clamp(0, newInd, states.length-1);
		tab = states[currInd];
		//if (oldTab) console.log('WHAT');
		// console.log('Animate');
		chartStartIndex = 0;
		updateBarsChart();
		updateBubbles();updateBubbles();		
		if (parseInt(tab.title.split(' ')[1]).toString() != 'NaN') {
			var measure = parseInt(tab.title.split(' ')[1]);
			console.log('MEASURED ' + measure);
			console.log(tab[measure]);
			selectedNode = selectedNode | 3;
			cursorNode = selectedNode;
			nodes[selectedNode].key = measure;
			selectionConfirmed = true;
			updateBarsChartWithSelection();
		}
	}
}

function updateButtons() {
	if (currInd <= 0) {
		buttonPrev.html("-START-");
	}
	else {
		buttonPrev.html("Previous state");
	}
	
	if (currInd >= states.length-1) {
		buttonNext.html("-END-");
	}
	else {
		buttonNext.html("Next state");
	}
	d3.select("#subtitle").html(tab.title);
}

function updateText() {return;
	var newText = '';
	
	if (typeof(selectedNode) != 'undefined') {
		newText += '<span class="red">';
		newText += tab[selectedNode].key;
		newText += '</span>';
		newText += ' (' + tab[selectedNode].key + ')';
	}
	
	if (typeof(cursorNode) != 'undefined') {
		if (newText.length > 0){
			newText += ' <b>-</b> ';
		}
	
		newText += '<span class="green">';
		newText += tab[cursorNode].key;
		newText += '</span>';
		newText += ' (' + tab[cursorNode].key + ')';
	}
	
	selectionText.html(newText);
}

var buttonPrev = d3.select("#controls").append("button").attr("class", 'block').html("-START-");
var selectionText = d3.select("#controls").append("h1").attr("class", 'block').html("-");
var buttonNext = d3.select("#controls").append("button").attr("class", 'block').html("Next state");

buttonPrev.on("click", function() {
	if (currInd > 0) {
		changeState(currInd-1);
		buttonNext.html("Next state");
	}
	
	updateButtons();
	
});

buttonNext.on("click", function() {
	if (currInd < states.length) {
		changeState(currInd+1);
		buttonPrev.html("Previous state");
	}
	
	updateButtons();
	
});


console.log('READY');





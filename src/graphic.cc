#include <iostream>
#include <SDL.h>
#include "graphic/display.hh"
#include "quantum/register.hh"
#include "quantum/expMul.hh"

const int SCREEN_WIDTH = 1240;
const int SCREEN_HEIGHT = 1080;

int deutschJozsa(Display& display);
int Shor(Display& display);

int getwidth(int N) {
  int i = 1;

  for(;1<<i < N; ++i);

  return i;
}

int main() {

  SDL_Window* window = nullptr;

  SDL_Surface* screenSurface = nullptr;

  if (SDL_Init( SDL_INIT_VIDEO) < 0) {
    std::cerr << "ERROR: could not start SDL" << std::endl;
    return 0;
  }

  window = SDL_CreateWindow("MyLittleDeutschJozsa", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);

  if (window == nullptr) {
    std::cerr << "ERROR: could not create window (SDL error #" << SDL_GetError() << ")" << std::endl;
    return 0;
  }

  screenSurface = SDL_GetWindowSurface(window);
  
  SDL_FillRect(screenSurface, nullptr, SDL_MapRGB(screenSurface->format, 0xFF, 0xFF, 0xFF));

  SDL_UpdateWindowSurface(window);
  SDL_UpdateWindowSurface(window);
  SDL_UpdateWindowSurface(window);
  SDL_UpdateWindowSurface(window);

  Display display(window, screenSurface);

  changeDisplay(&display);
  
  deutschJozsa(display);
  //Shor(display);
  
  SDL_DestroyWindow(window);
  SDL_Quit();

  return 0;
}

int deutschJozsa(Display& display) {
  int bitsNum = 14;
  
  quReg reg(bitsNum + 1);
  reg.setAmplitude((1 << (bitsNum)), 1);

  // std::cout << "Starting with " << std::endl << reg << std::endl;
  display.draw(reg, 2000);

  for (int i = 0; i < bitsNum + 1; ++i) {
    reg.HADAMARD(i);
    display.draw(reg, 2000);
  }
  
  //std::cout << "HADAMARD on the input " << std::endl << reg << std::endl;

  display.draw(reg, 2000);

  auto lambda = [&bitsNum](long a) {
    int choice = 0;

    switch (choice) {
    case 0:
      return 0;
    case 1:
      return 1;
    case 2:
      return (int)(a < (1 << (bitsNum - 1)));
    case 3:
      return (int)(a >= (1 << (bitsNum - 1)));
    }
    
    return 0;
  };

  reg.applyFunction(bitsNum, lambda);

  //std::cout << "Applied the oracle " << std::endl << reg << std::endl;

  display.draw(reg, 2000);

  for (int i = 0; i < bitsNum; ++i) {
    reg.HADAMARD(i);
  }

  // std::cout << "HADAMARD on the input " << std::endl << reg << std::endl;

  display.draw(reg, 2000);

  int result = 0;

  for (int i = 0; i < bitsNum; ++i) {
    result += (reg.measureQuBit(i)) << i;
  }

  if (result == 0) {
    std::cout << "The function is constant" << std::endl;
  }
  else {
    std::cout << "The function is balanced" << std::endl;
  }

  // std::cout << "The input was measured at " << result << std::endl << reg << std::endl;

  display.draw(reg, 2000);

}

int Shor(Display& display) {
  
}
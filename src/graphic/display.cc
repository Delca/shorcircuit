#include "display.hh"

Display::Display(SDL_Window* wind, SDL_Surface* surf) {
  this->window = wind;
  this->screenSurface = surf;

  states[0] = 0.2;
  states[2] = -0.3;
  states[3] = 0.5;

  verticalPos[0] = 0;
  verticalPos[2] = 1;
  verticalPos[3] = 2;
}

void Display::initStates(std::map<long, double>& map) {
  states.clear();
  verticalPos.clear();

  long count = 0;

  for (auto ite = map.begin(); ite != map.end(); ++ite) {
    states[ite->first] = ite->second;
    verticalPos[ite->first] = count++;
  }

}

void Display::drawBar(int delay = 0) {
  SDL_Rect rect;
  rect.x = centerX;
  rect.y = centerY;
  rect.h = totalSize / states.size();
  rect.h = std::min(rect.h, 30);
  rect.w = 10;

  SDL_FillRect(screenSurface, nullptr, SDL_MapRGB(screenSurface->format, 0xFF, 0xFF, 0xFF));

  for(auto ite = states.begin(); ite != states.end(); ++ite) {
    rect.x = centerX;
    rect.y = centerY + verticalPos[ite->first] * rect.h;
    rect.w = states[ite->first] * length;
    
    if (rect.w < 0) {
      rect.w *= -1;
      rect.x -= rect.w;
    }

    SDL_FillRect(screenSurface, &rect, SDL_MapRGB(screenSurface->format, 0xFF, 0x00, 0xCC*verticalPos[ite->first]));

  }

  SDL_UpdateWindowSurface(window);

  SDL_Delay(delay);
}

void Display::drawSquare(int delay = 0) {
  SDL_Rect rect;
  rect.x = offX;
  rect.y = offY;
  rect.h = stateH;
  rect.w = stateW;
  
  SDL_FillRect(screenSurface, nullptr, SDL_MapRGB(screenSurface->format, 0xFF, 0xFF, 0xFF));
  
  long lineLength = 1 << (regSize / 2);
  
  std::cout << "LINE LENGTH:" << lineLength << std::endl;
  
  for(auto ite = states.begin(); ite != states.end(); ++ite) {
    long ind = ite->first;
    
    rect.x = offX + (ind % lineLength) * stateW;
    rect.y = offY + (ind / lineLength) * stateH;

    int colourInd = (ind % lineLength)%2 + (ind / lineLength)%2;
    colourInd %= 2;
    
    SDL_FillRect(screenSurface, &rect, SDL_MapRGB(screenSurface->format, 0xFF*colourInd, 0x00, 0xFF*(1-colourInd)));
  }
  
  
  SDL_UpdateWindowSurface(window);

  SDL_Delay(delay);  
}

void Display::draw(quReg& reg, int delay = 0) {
  initStates(reg);
  drawSquare(delay);
  
  //std::cout << reg;
    
}

void Display::initStates(quReg& reg) {
  std::map<long, double> map;
  regSize = reg.getSize();

  amplitude** regStates = reg.getStates();
  int size = reg.getSize();

  for(long i = 0; i < (1 << size); ++i) {
    if (regStates[i] != nullptr) {
      map[i] = std::abs(*regStates[i]);
    }
  }

  initStates(map);
}

void Display::changeTitle(std::string newTitle) {
  SDL_SetWindowTitle(window, newTitle.c_str());
}

void Display::registerState(std::map<long, double>& map) {
  json << "{";

  
  for(auto ite = states.begin(); ite != states.end(); ++ite) {
        
  }
  
  
  
  
  json << "}";
  
  if (stateNum == 0) {
   json << ","; 
  }
  
}
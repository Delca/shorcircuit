#ifndef __DISPLAY__HH__
#define __DISPLAY__HH__

#include <SDL.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <map>

#include "../quantum/register.hh"

class Display {
private:

  SDL_Window* window;
  SDL_Surface* screenSurface;
  std::map<long, double> states;
  std::map<long, long> verticalPos;
  const int centerX = 300-1;
  const int centerY = 2;
  const int length = 300;
  const int totalSize = 396;
  int regSize;
  int stateNum;
  std::ostringstream json;
  
  const int offX = 10;
  const int offY = 10;
  const int stateW = 1;
  const int stateH = 1;
  
  
  
public:

  Display(SDL_Window* wind, SDL_Surface* surf);

  void initStates(std::map<long, double>& map);
  void initStates(quReg& reg);
  void drawBar(int delay);
  void drawSquare(int delay);
  void draw(quReg& reg, int delay);
  void changeTitle(std::string newTitle);
  
  void registerState(std::map<long, double>& map);

};

#endif

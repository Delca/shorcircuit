#include "json.hh"
#include "../quantum/register.hh"

Json::Json() {
  buffer << "var states = [";
  count = 0;
}

void Json::record(quReg& reg) {
  record(reg, "Etape #" + std::to_string(count));
}

void Json::record(quReg& reg, std::string title) {
  if (buffer.str().size() > 14) {
    buffer << ", ";
  }

  buffer << "{";
  buffer << "\"title\": \"" << title << "\",";

  std::vector<partialState> states;

  for (int i = 0; i < reg.maxUsed; ++i) {
    if (reg.amps[i] != amplitude(0)) {
      states.push_back(partialState(reg.states[i], reg.amps[i]));
    }
  }
  auto lambda = [](partialState s1, partialState s2){
    return std::abs(s1.second) < std::abs(s2.second);
  };
  std::sort(states.begin(), states.end(), lambda);
  int maxStates = std::min((int)states.size(), 8192);
  for(int i = 0; i < maxStates; ++i) {
    buffer << " \"" << states[i].first << "\": {";

    buffer << "\"key\": \"" << states[i].first << "\", ";
    buffer << "\"amp\": \"" << std::abs(states[i].second) << "\", ";
    buffer << "\"real\": \"" << states[i].second.real() << "\", ";
    buffer << "\"imag\": \"" << states[i].second.imag() << "\"";
    buffer << "},";
  }

  buffer << " \"keys\": [";
  for(int i = 0; i < maxStates; ++i) {
    buffer << (i>0?", ":"") << states[i].first;
  }

  buffer << "], \"size\":\"" << maxStates << "\"";
  buffer << "}";
}

void Json::writeToStream(std::ostream& file) {
  file << buffer.str();
  file << "];";
}

void Json::writeToFile(std::string fileName) {
  std::ofstream file;

  file.open(fileName);

  if (file.is_open()) {
    writeToStream(file);
    file.close();
    return;
  }

  std::cerr << "Could not open file " << fileName << std::endl;
}

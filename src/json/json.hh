#ifndef __JSON__HH__
#define __JSON__HH__

#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <algorithm>

class quReg;

class Json {
private:

  int count;
  std::ostringstream buffer;

public:

  Json();

  void record(quReg& reg);
  void record(quReg& reg, std::string title);
  void writeToStream(std::ostream& o);
  void writeToFile(std::string fileName);

};

#endif

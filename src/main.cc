#include <iostream>
#include <ctime>

#include "quantum/register.hh"
#include "quantum/expMul.hh"
#include "json/json.hh"

int getwidth(int N) {
  int i = 1;

  for(;1<<i < N; ++i);

  return i;
}

void
quantum_frac_approx(int *a, int *b, int width)
{
  float f = (float) *a / *b;
  float g=f;
  int i, num2=0, den2=1, num1=1, den1=0, num=0, den=0;
 
  do
    {
      i = (int) (g+0.000005);
      
      g -= i-0.000005;
      g = 1.0/g;

      if (i * den1 + den2 > 1<<width)
	break;

      num = i * num1 + num2;
      den = i * den1 + den2;

      num2 = num1;
      den2 = den1;
      num1 = num;
      den1 = den;

    } while(fabs(((double) num / den) - f) > 1.0 / (2 * (1 << width)));
  
  *a = num;
  *b = den;

  return;
}

int
quantum_gcd(int u, int v)
{
  int r;

  while(v)
    {
      r = u % v;
      u = v;
      v = r;
    }
  return u;
}

int main_SHOR(int N = 15, int x = 14) {
  while (x == 1 || quantum_gcd(x, N) != 1) {
    x = rand() % N;
  }

  int width = getwidth(N*N);
  int swidth = getwidth(N);

  int totalWidth = width + 3*swidth+2;

  quReg reg(width);
  reg.setAmplitude(0, 1);
  reg.record("Initial state, trying to factorize " + std::to_string(N));
  std::cout << "reg size " << width + 3*swidth+2 << std::endl;

  for(int i = 0; i < width; ++i) {
    clock_t start = clock();
    std::cout << "HADAMARD on " << i << std::endl;
    reg.HADAMARD(i);
    std::cout << "Took " << (float)(clock()-start) / CLOCKS_PER_SEC << std::endl;
  }
  reg.record("HADAMARD on all " + std::to_string(width) + " bits of the input");

  reg.addBitsToRegister(3*swidth+2);

  reg.record("Adding temporary bits from the register");

  expModN(reg, N, x, width, swidth);
  reg.record(std::to_string(x) + " to the power of the register modulo " + std::to_string(N));

  std::cout << "Measuring register" << std::endl;
  
  for(int i = 0; i < 3*swidth+2; ++i) {
      reg.measureQuBit(i);
      //reg.record("Measured bit " + std::to_string(i));
  }

  reg.record("Measure of the output (temporary) bits");


  std::cout << "Removing bits from register" << std::endl;

  reg.removeBitsFromRegister(3*swidth+2);

  reg.record("Removing temporary bits from the register");

  std::cout << "Applying QFT" << std::endl;
  
  reg.QFT(width, 0);
  
  for (int i = 0; i < width/2; ++i) {
   reg.CNOT(i, width-i-1);
   reg.CNOT(width-i-1, i);
   reg.CNOT(i, width-i-1);
  }
  
  reg.record("Applying a Quantum Fourier Transform to the register");

  for (int i = 0; i < 32; ++i) {
    int measured = reg.measure_NO_COLLAPSE();
    int measured_SAVE = measured;
    std::cout << "Measured " << measured << ", ";

    int p = (1 << width);

    quantum_frac_approx(&measured, &p, width);
    
    std::cout << "Fractional approximation " << measured << " / " << p << ", ";

    if (p % 2 == 1) {
      std::cout << "ODD PERIOD" << std::endl;
      continue;
    }

    int firstFactor = 0;
    
    int sq1 = std::sqrt(std::pow(x, p/2)) + 1;
    int sq2 = std::sqrt(std::pow(x, p/2)) - 1;

    sq1 = quantum_gcd(N, sq1);
    sq2 = quantum_gcd(N, sq2);

    firstFactor = std::max(sq1, sq2);

    if (firstFactor > 1 && firstFactor < N) {
      std::cout << N << " = " << firstFactor << " * " << N/firstFactor << std::endl;
      reg.record("Measured " + std::to_string(measured_SAVE) + " , which gave us the period " + std::to_string(p) + " , thus<br> " + std::to_string(N) + " = " + std::to_string(firstFactor) + " * " + std::to_string((int)N/firstFactor));
      reg.saveToFile("js/states.json");
      return 0;
    }

    std::cout << "Could not factorize " << N << std::endl;

  }

  reg.record("The number could not be factorized, please try again");
  reg.saveToFile("js/states.json");
  
}

/* --------------------------------- */
/* --------------------------------- */
/* --------------------------------- */
/* --------------------------------- */

int main_DJ(int bitsNum = 5, int choice = 1) {

  quReg reg(bitsNum + 1);
  reg.setAmplitude((1 << (bitsNum)), 1);

  std::cout << "Starting with " << std::endl << reg << std::endl;
  reg.record("Initial state");

  for (int i = 0; i < bitsNum + 1; ++i) {
    reg.HADAMARD(i);
    std::cout << "HADAMARD ON " << i << "\n";
    std::cout << reg;
    reg.record("Hadamard on bit " + std::to_string(i));
  }
  
  std::cout << "HADAMARD on the input " << std::endl << reg << std::endl;

  //return 0;
  
  auto lambda = [bitsNum, choice](long a) {
    switch (choice) {
    case 0:
      return 0;
    case 1:
      return 1;
    case 2:
      return (int)(a < (1 << (bitsNum - 1)));
    case 3:
      return (int)(a >= (1 << (bitsNum - 1)));
    }
    
    return 0;
  };

  reg.applyFunction(bitsNum, lambda);
  reg.record("Applying the oracle");

  std::cout << "Applied the oracle " << std::endl << reg << std::endl;

  for (int i = 0; i < bitsNum; ++i) {
    reg.HADAMARD(i);
    reg.record("Hadamard on bit " + std::to_string(i));
  }

  std::cout << "HADAMARD on the input " << std::endl << reg << std::endl;

  int result = 0;

  for (int i = 0; i < bitsNum; ++i) {
    result += (reg.measureQuBit(i)) << i;
    reg.record("Measure on bit " + std::to_string(i));
  }

  if (result == 0) {
    std::cout << "The function is constant" << std::endl;
    reg.record("The function is constant");
  }
  else {
    std::cout << "The function is balanced" << std::endl;
    reg.record("The function is balanced");
  }

  std::cout << "The input was measured at " << result << std::endl << reg << std::endl;

  reg.saveToFile("js/states.json");
}

void usage(std::string name) {
    std::cout << "Usage: "<< name << "[-dj [B] [C]] [-shor N [a]]" << std::endl;
    std::cout << "\t-dj [B: number of bits of the input register] [C: 0,1 for constant oracle; 2, 3 for balanced oracle]" << std::endl;
    std::cout << "\t-shor N:number to factorize [a:initial random number]" << std::endl;
    exit(1);
}

int main(int argc, char** argv) {
  srand(time(nullptr));
  
  if (argc < 2) {
    usage(std::string(argv[0]));
  }
  
  if (std::string(argv[1]) == "-dj") {
    int b = (argc > 2?std::atoi(argv[2]):5);
    int c = (argc > 3?std::atoi(argv[3]):1);
    main_DJ(b, c);
    return 0;
  }
  
  if (argc > 2 && std::string(argv[1]) == "-shor") {
    int N = std::atoi(argv[2]);
    int a = (argc>3?std::atoi(argv[3]):N);
    main_SHOR(N, a);
    return 0;
  }
  
  usage(std::string(argv[0]));
}

#include "expMul.hh"

int num_regs = 4;

void changeDisplay(Display* d) {theDisplay = d;}

int inverseMod(int n, int c) {
  int i = 1;

  for(;(i*c)%n!=1; ++i);

  return i;
}

//This is a semi-quantum fulladder. It adds to b_in
//a c-number. Carry-in bit is c_in and carry_out is
//c_out. xlt-l and L are enablebits. See documentation
//for further information

void muxfa(quReg& reg, int a, int  b_in, int c_in, int c_out, int xlt_l,int L, int total){//a,

  if(a==0){//00
  reg.TOFFOLI(b_in,c_in,c_out);
  reg.CNOT(b_in,c_in);
  }

  if(a==3){//11
  reg.TOFFOLI(L,c_in,c_out);
  reg.CNOT(L,c_in);
  reg.TOFFOLI(b_in,c_in,c_out);
  reg.CNOT(b_in,c_in);
  }

  if(a==1){//01
  reg.TOFFOLI(L,xlt_l,b_in);
  reg.TOFFOLI(b_in,c_in,c_out);
  reg.TOFFOLI(L,xlt_l,b_in);
  reg.TOFFOLI(b_in,c_in,c_out);
  reg.TOFFOLI(L,xlt_l,c_in);
  reg.TOFFOLI(b_in,c_in,c_out);
  reg.CNOT(b_in,c_in);
  }


  if(a==2){//10
  reg.NOT(xlt_l);
  reg.TOFFOLI(L,xlt_l,b_in);
  reg.TOFFOLI(b_in,c_in,c_out);
  reg.TOFFOLI(L,xlt_l,b_in);
  reg.TOFFOLI(b_in,c_in,c_out);
  reg.TOFFOLI(L,xlt_l,c_in);
  reg.TOFFOLI(b_in,c_in,c_out);
  reg.CNOT(b_in,c_in);
  reg.NOT(xlt_l);
  }
}

//This is just the inverse operation of the semi-quantum fulladder

void muxfa_inv(quReg& reg, int a,int  b_in,int c_in,int c_out, int xlt_l,int L,int total){//a,

  if(a==0){//00
  reg.CNOT(b_in,c_in);
  reg.TOFFOLI(b_in,c_in,c_out);
  }

  if(a==3){//11
  reg.CNOT(b_in,c_in);
  reg.TOFFOLI(b_in,c_in,c_out);
  reg.CNOT(L,c_in);
  reg.TOFFOLI(L,c_in,c_out);
  }

  if(a==1){//01
  reg.CNOT(b_in,c_in);
  reg.TOFFOLI(b_in,c_in,c_out);
  reg.TOFFOLI(L,xlt_l,c_in);
  reg.TOFFOLI(b_in,c_in,c_out);
  reg.TOFFOLI(L,xlt_l,b_in);
  reg.TOFFOLI(b_in,c_in,c_out);
  reg.TOFFOLI(L,xlt_l,b_in);
  }


  if(a==2){//10
  reg.NOT(xlt_l);
  reg.CNOT(b_in,c_in);
  reg.TOFFOLI(b_in,c_in,c_out);
  reg.TOFFOLI(L,xlt_l,c_in);
  reg.TOFFOLI(b_in,c_in,c_out);
  reg.TOFFOLI(L,xlt_l,b_in);
  reg.TOFFOLI(b_in,c_in,c_out);
  reg.TOFFOLI(L,xlt_l,b_in);
  reg.NOT(xlt_l);
  }
}

//This is a semi-quantum halfadder. It adds to b_in
//a c-number. Carry-in bit is c_in and carry_out is
//not necessary. xlt-l and L are enablebits. See
//documentation for further information

void muxha(quReg& reg, int a,int  b_in,int c_in, int xlt_l, int L,int total){//a,

  if(a==0){//00
  reg.CNOT(b_in,c_in);
  }

  if(a==3){//11
  reg.CNOT(L,c_in);
  reg.CNOT(b_in,c_in);
  }

  if(a==1){//01
  reg.TOFFOLI(L,xlt_l,c_in);
  reg.CNOT(b_in,c_in);
  }


  if(a==2){//10
  reg.NOT(xlt_l);
  reg.TOFFOLI(L,xlt_l,c_in);
  reg.CNOT(b_in,c_in);
  reg.NOT(xlt_l);
  }
}

//just the inverse of the semi quantum-halfadder

void muxha_inv(quReg& reg, int a,int  b_in,int c_in, int xlt_l, int L, int total){//a,

  if(a==0){//00
  reg.CNOT(b_in,c_in);
  }

  if(a==3){//11
  reg.CNOT(b_in,c_in);
  reg.CNOT(L,c_in);
  }

  if(a==1){//01
  reg.CNOT(b_in,c_in);
  reg.TOFFOLI(L,xlt_l,c_in);
  }


  if(a==2){//10
  reg.NOT(xlt_l);
  reg.CNOT(b_in,c_in);
  reg.TOFFOLI(L,xlt_l,c_in);
  reg.NOT(xlt_l);
  }
}

void mAdd(quReg& reg, int a,int a_inv,int  width){
	int i,j;
	int total;
	total = num_regs*width+2;
	for (i = 0; i< width-1; i++){
		if((1<<i) & a) j= 1<<1;
	  	  else j=0;
		if((1<<i) & a_inv) j+=1;
		muxfa(reg, j,width+i,i,i+1,2*width,2*width+1, total);
		}
	j=0;
	if((1<<(width-1)) & a) j= 2;
	if((1<<(width-1)) & a_inv) j+=1;
	muxha(reg, j,2*width-1,width-1,2*width,2*width+1, total);
}

void mAddInv(quReg& reg, int a,int a_inv,int  width){
	int i,j;
	int total;
	total = num_regs*width+2;
	j=0;

	if((1<<(width-1)) & a) j= 2;
	if((1<<(width-1)) & a_inv) j+=1;
	muxha_inv(reg, j,width-1,2*width-1,2*width, 2*width+1, total);

	for (i = width-2; i>=0; i--){
		if((1<<i) & a) j= 1<<1;
	  	  else j=0;
		if((1<<i) & a_inv) j+=1;
		muxfa_inv(reg, j,i,width+i,width+1+i,2*width, 2*width+1, total);
		}
}

void testSum(quReg& reg, int compare, int width) {
  int i;

  if (compare & ((long) 1 << (width - 1)))
    {
      reg.CNOT(2*width-1, width-1);
      reg.NOT(2*width-1);
      reg.CNOT(2*width-1, 0);
    }
  else
    {
      reg.NOT(2*width-1);
      reg.CNOT(2*width-1,width-1);
    }
  for (i = (width-2);i>0;i--)
    {
      if (compare & (1<<i))
	{//is bit i set in compare?
	  reg.TOFFOLI(i+1,width+i,i);
	  reg.NOT(width+i);
	  reg.TOFFOLI(i+1,width+i,0);
	}
      else
	{
	  reg.NOT(width+i);
	  reg.TOFFOLI(i+1,width+i,i);
	}
    }
  if (compare & 1) 
    {
      reg.NOT(width);
      reg.TOFFOLI(width,1,0);
    }
  reg.TOFFOLI(2*width+1,0,2*width);//set output to 1 if enabled and b < compare

  if (compare & 1) 
    {
      reg.TOFFOLI(width,1,0);
      reg.NOT(width);
    }

  for (i = 1;i<=(width-2);i++)
    {
      if (compare & (1<<i))
	{//is bit i set in compare?
	  reg.TOFFOLI(i+1,width+i,0);
	  reg.NOT(width+i);
	  reg.TOFFOLI(i+1,width+i,i);
	}
      else
	{
	  reg.TOFFOLI(i+1,width+i,i);
	  reg.NOT(width+i);
	}
    }
  if (compare & (1<<(width-1)))
    {
      reg.CNOT(2*width-1,0);
      reg.NOT(2*width-1);
      reg.CNOT(2*width-1,width-1);
    }
  else
    {
      reg.CNOT(2*width-1,width-1);
      reg.NOT(2*width-1);
       }
}

void swapTheLead(quReg& reg, int width) {
  for (int i = 0; i < width; ++i) {
    reg.CNOT(i, width+i);
    reg.CNOT(width+i, i);
    reg.CNOT(i, width+i);
  }
}

void addModN(quReg& reg, int N, int a, int width) {
  //std::cout << "ADD MOD N" << std::endl;
  testSum(reg, N - a, width);
  mAdd(reg, (1<<width)+a-N, a, width);

  reg.CNOT(2*width+1, 2*width);
  mAddInv(reg, (1<<width)-a, N-a, width);
  swapTheLead(reg, width);
  testSum(reg, a, width);
}

void emul(quReg& reg, int a, int L, int width) {
  for (int i = width-1; i >= 0; --i) {
    if ((a>>i) & 1) {
      reg.TOFFOLI(2*width+2, L, width+i);
    }
  }
}

void mulNInv(quReg& reg, int N, int a, int ctl, int width) {
  int L = 2*width + 1;

  a = inverseMod(N, a);

  for(int i = width-1; i > 0; --i) {
    reg.TOFFOLI(ctl, 2*width+2+i, L);
    addModN(reg, N, N-((1<<i) * a) % N, width);
    reg.TOFFOLI(ctl, 2*width+2+i, L);
  }

  reg.TOFFOLI(ctl, 2*width+2, L); 
  emul(reg, a%N, L, width);
  reg.TOFFOLI(ctl, 2*width+2, L); 
}

void mulN(quReg& reg, int N, int a, int ctl, int width) {
  int L = 2*width+1;

  reg.TOFFOLI(ctl, L+1, L);
  emul(reg, a%N, L, width);
  reg.TOFFOLI(ctl, L+1, L);

  for(int i = 1; i < width; ++i) {
    reg.TOFFOLI(ctl, 2*width+2+i, L);
    addModN(reg, N, ((1<<i) * a) % N, width);
    reg.TOFFOLI(ctl, 2*width+2+i, L);
  }

}

void C_swapTheLead(quReg& reg, int control, int width) {
  for (int i = 0; i < width; ++i) {
    reg.TOFFOLI(control, width+i, 2*width+i+2);
    reg.TOFFOLI(control, 2*width+i+2, width+i);
    reg.TOFFOLI(control, width+i, 2*width+i+2);
  }
}

void mulModN(quReg& reg, int N, int a, int ctl, int width) {
  //std::cout << "MUL MOD N" << std::endl;
  mulN(reg, N, a, ctl, width);
  
  C_swapTheLead(reg, ctl, width);

  mulNInv(reg, N, a, ctl, width);
  
  if (false && theDisplay != nullptr) {
   theDisplay->draw(reg, 2000); 
   theDisplay->changeTitle("Multiplication modulo N");
  }
  
}

void expModN(quReg& reg, int N, int x, int width, int swidth) {
  //std::cout << "EXP MOD N" << std::endl;
  // Setting the control bit
  reg.NOT(2*swidth+2);
  
  //std::cout << "CONTROL BIT SET" << std::endl;
  
  int f = 0;

  for (int i = 1; i <= width; ++i) {
    f = x % N;
    for (int j = 1; j < i; ++j) {
      f *= f;
      f %= N;
    }
    mulModN(reg, N, f, 3*swidth+1+i, swidth);

  }

}

#ifndef __EXPMUL__HH__
#define __EXPMUL__HH__

#include "register.hh"
#include "../graphic/display.hh"

static Display* theDisplay = nullptr;

void changeDisplay(Display* d);
void expModN(quReg& reg, int N, int x, int width, int swidth);

#endif

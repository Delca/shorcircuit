#include "register.hh"

quReg::quReg(int size) {
  this->size = size;
  //states = (amplitude**) calloc( 1 << size, sizeof(amplitude*));
  //if (states == nullptr) {
  // std::cerr << "ERROR: could not allocate register" << std::endl; 
  //}
  // states[0] = new amplitude(1, 0);
  
  internalSize = 1000 * 1000;
  maxUsed = 0;
  
  amps = (amplitude*) calloc(internalSize, sizeof(amplitude));
  states = (long*) calloc(internalSize, sizeof(long));
  
}

quReg::~quReg() {
  free(states);
  free(amps);
}

std::ostream& operator<<(std::ostream& o, quReg& q) {
  q.print(o);
  return o;
}

void quReg::clear() {
//   for(long i = 0; i < ((long)1 << size); ++i) {
//     if (states[i] != nullptr) {
//       delete states[i];
//     }
//   }
//   free(states);
//   states = (amplitude**) calloc((long)1 << size, sizeof(amplitude));
  
  // TODO CLEAR STATES
  
  
}

void quReg::print(std::ostream& o) {
  o << "Register of size " << this->size << std::endl;

  long count = 0;
  
  for(long i = 0; i < internalSize; ++i) {
      if (amps[i] != amplitude(0)) {
	++count;
	regState s = states[i];
	o << amps[i] << " {" << std::pow(std::abs(amps[i]), 2);
	o << "} |";
	for(int i = size-1; i >= 0; --i) {
	  o << ((s & ((long)1 << i)) >> i) << (i%4==0?" ":"");
	}
	o << "> (" << s << ")" << std::endl;
      }
    }

  std::cout << count << " present states" << std::endl;

  // for(partialState pS : states) {
  //   regState s = pS.first;
  //   o << pS.second << " |";
  //   for(int i = size-1; i >= 0; --i) {
  //     o << ((s & ((long)1 << i)) >> i);
  //   }
  //   o << "> (" << s << ")" << std::endl;
  // }
}

long quReg::setAmplitude(partialState pS) {
  long firstSpot = -1;
  long i = 0;
  
  for(i = 0; i < maxUsed; ++i) {
   if (firstSpot == -1 && amps[i] == amplitude(0)) {
    firstSpot = i; 
   }
   
   if (states[i] == pS.first) {
    firstSpot = i;
    break; 
   }
   
  }
  
  if (i == maxUsed) {
    if (firstSpot == -1) {
      if (i == internalSize) {
	// Need to reallocate space for the register
        std::cout << "NEEDS MORE SPACE" << std::endl;
	exit(0);
      }
      else {
	//std::cout << "USING A NEW SPACE AT THE END for "<< pS.first << std::endl;
	firstSpot = maxUsed;
	++maxUsed;
      }	
    }
  }
  
  states[firstSpot] = pS.first;
  amps[firstSpot] = pS.second;

  return firstSpot;
}

amplitude quReg::getAmplitude(regState rS) {
  for(long i = 0; i < internalSize; ++i) {
    if (states[i] == rS) {
     return amps[i]; 
    }
  }
  
  return amplitude(0);
}

void quReg::NOT(int ind) {
  //Eigen::MatrixXcd gate;

//   for (long i = 0; i < ((long)1 << (size-1)); ++i) {break;
//     //std::cout << i << std::endl;
//     regState patternR = i;
//     shiftAtBit(patternR, ind);
// 
//     regState state0 = patternR;
//    
//     setBitAt(patternR, ind, 1);
//     regState state1 = patternR;
// 
//     amplitude amplitude0 = getAmplitude(state0);
//     amplitude amplitude1 = getAmplitude(state1);
// 
//     setAmplitude(partialState(state0, amplitude1));
//     setAmplitude(partialState(state1, amplitude0));
// 
//   }
  
  for(long i = 0; i < maxUsed; ++i) {
    states[i] ^= ((long)1 << ind);
  }    

}

long quReg::getIndex(regState rState) {
  for(int i = 0; i < maxUsed; ++i) {
    if (states[i] == rState) {
      return i;
    }
  }

  return -1;
}

void quReg::HADAMARD(int ind) {
  //Eigen::MatrixXcd gate;

//   for (long i = 0; i < ((long)1 << (size-1)); ++i) {break;
//     regState patternR = i;
//     shiftAtBit(patternR, ind);
// 
//     regState state0 = patternR;
//    
//     setBitAt(patternR, ind, 1);
//     regState state1 = patternR;
// 
//     amplitude amplitude0 = getAmplitude(state0);
//     amplitude amplitude1 = getAmplitude(state1);
// 
//     constexpr double normalize = 1/std::sqrt(2);
// 
//     setAmplitude(partialState(state0, normalize*(amplitude0 + amplitude1)));
//     setAmplitude(partialState(state1, normalize*(amplitude0 - amplitude1)));
// 
//   }
    
  bool* done = (bool*) calloc(internalSize, sizeof(bool));
  
  for(long i = 0; i < maxUsed; ++i) {;
   if (done[i]) {
     continue;
   }
   
   constexpr double normalize = 1/std::sqrt(2);
   
   regState r1 = states[i];
   regState r2 = r1;
   int setValue = !(r1 & ((long)1 << ind));
   setBitAt(r2, ind, !(r1 & ((long)1 << ind)));
   
   long j = -1;
   for(long ji = 0; ji < maxUsed; ++ji) {
     if (states[ji] == r2) {
       j = ji;
       break;
     }
   }
   
   if (j == -1)  {
     setAmplitude(r2, 0);
     done = (bool*) realloc(done, internalSize * sizeof(bool));
     for(long ji = 0; ji < maxUsed; ++ji) {
     if (states[ji] == r2) {
       j = ji;
       break;
       }
     }
   }
   
   if (!setValue) {
     // r1 has the bit set to 1, we should swap the regState
     regState temp = r1;
     r1 = r2;
     r2 = temp;
     long temp2 = j;
     j = i;
     i = temp2;
   }

   amplitude amplitude0 = (i!=-1?amps[i]:0);
   amplitude amplitude1 = (j!=-1?amps[j]:0);
   
   amplitude new0 = normalize*(amplitude0 + amplitude1);
   amplitude new1 = normalize*(amplitude0 - amplitude1);
   
   setAmplitude(r1, new0);
   setAmplitude(r2, new1);
   
   done[i] = true;
   done[j] = true;

   if (!setValue) {
     i = j;
   }
  }
  
  free(done);
}

void quReg::CNOT(int c, int t) {
  //Eigen::MatrixXcd gate;
  //c = (size-1)-c;
  //t = (size-1)-t;

//   for (long i = 0; i < ((long)1 << (size-2)); ++i) {break;
//     //std::vector<int> pattern = getStateAsList(i, this->size-2);
//     regState patternR = i;
//     
//     //pattern.insert(pattern.begin() + std::min(c, t), 0);
//     //pattern.insert(pattern.begin() + std::max(c, t), 0);   
//     shiftAtBit(patternR, std::min(c, t));
//     shiftAtBit(patternR, std::max(c, t));
//     regState state00 = patternR;
//    
//     //pattern[t] = 1;
//     setBitAt(patternR, t, 1);
//     regState state01 = patternR;
//  
//     //pattern[t] = 0;
//     //pattern[c] = 1;
//     setBitAt(patternR, t, 0);
//     setBitAt(patternR, c, 1);
//     regState state10 = patternR;
// 
//     //pattern[t] = 1;
//     setBitAt(patternR, t, 1);
//     regState state11 = patternR;
// 
//     amplitude amplitude10 = getAmplitude(state10);
//     amplitude amplitude11 = getAmplitude(state11);
// 
//     setAmplitude(state11, amplitude10);
//     setAmplitude(state10, amplitude11);
// 
//   }
  
  for(long i = 0; i < maxUsed; ++i) {
    if (states[i] & ((long)1 << c)) {
      states[i] ^= ((long)1 << t);
    }
  }

}

int quReg::measureQuBit_NO_COLLAPSE(int ind) {
  double probabilityOf0 = 0;
  // ind = (size-1)-ind;

//   for (long i = 0; i < ((long)1 << (size-1)); ++i) {break;
//     // std::vector<int> pattern = getStateAsList(i, this->size-1);    
// 
//     regState patternR = i;
//     shiftAtBit(patternR, ind);
//     // pattern.insert(pattern.begin() + ind, 0);
//     probabilityOf0 += std::pow(std::abs(getAmplitude(patternR)), 2);
//   }
  
  long count = 0;

  for(long i = 0; i < maxUsed; ++i) {
    if (!(states[i] & ((long)1 << ind))) {
      if (amps[i] != amplitude(0)) {
	probabilityOf0 += std::pow(std::abs(amps[i]) , 2);
	++count;
      }
    }
    else {
      if (amps[i] != amplitude(0)) {
	if (false) {
	  for(int ite = size-1; ite >= 0; --ite) {
	    std::cout << ((states[i] & ((long)1 << ite)) >> ite);
	  }
	  std::cout << std::endl;
	  for(int ite = size-1; ite >= 0; --ite) {
	    std::cout << ((((long)1 << ind) & ((long)1 << ite)) >> ite);
	  }
	  std::cout << "\n-------------------------------------\n";
	}
      }
    }
  }

  double rand = getRand();

  std::cout << "MEASURE OF 0 AT " << ind << ": " << probabilityOf0 << " from " << count << " states" << std::endl;
  std::cout << "RAND " << rand << std::endl;

  return (rand < probabilityOf0)?0:1;
}

int quReg::measureQuBit(int ind) { std::cout << "\n--MEASURING BIT " << ind << "--" << std::endl;
  double totalProb = 0;
  // First, measure the bit
  int measure = measureQuBit_NO_COLLAPSE(ind);
  // ind = (size-1)-ind;

  // Then, collapse the register so that
  // all states where the ind-bit is different
  // from the measure have a null amplitude

  double sumOfValidStates = 0;
  long invCount = 0;
  std::vector<long> validStates;

  // for (long i = 0; i < ((long)1 << (size-1)); ++i) {break;
  //   // std::vector<int> pattern = getStateAsList(i, this->size-1);

  //   regState patternR = i;

  //   shiftAtBit(patternR, ind);
  //   // pattern.insert(pattern.begin() + ind, measure);
  //   regState stateM = patternR;
  //   validStates.push_back(stateM);

  //   setBitAt(patternR, ind, 1-measure);
  //   // pattern[ind] = 1-measure;
  //   regState stateNM = patternR;
  //   sumOfInvalidStates += pow(std::abs(getAmplitude(stateNM)), 2);
  //   setAmplitude(stateNM, 0);
  // }
  
  long startStates = 0;

  for(long i = 0; i < maxUsed; ++i) {
    if (amps[i] == amplitude(0)) continue;

    ++startStates;

    if (states[i] & ((long)1 << ind)) {
      if (measure) {
	// Valid
        validStates.push_back(i);
	sumOfValidStates += pow(std::abs(amps[i]), 2);
      }
      else {
	// Invalid
	amps[i] = amplitude(0);
	++invCount;
      }
    }
    else {
      if (!measure) {
	// Valid
        validStates.push_back(i);
	sumOfValidStates += pow(std::abs(amps[i]), 2);
      }
      else {
	// Invalid
	amps[i] = amplitude(0);
	++invCount;
	
	if (false && ind == 15) {
	  for(int ite = size-1; ite >= 0; --ite) {
	    std::cout << ((states[i] & ((long)1 << ite)) >> ite);
	  }
	  std::cout << std::endl;
	  for(int ite = size-1; ite >= 0; --ite) {
	    std::cout << ((((long)1 << ind) & ((long)1 << ite)) >> ite);
	  }
	  std::cout << "\n-------------------------------------\n";
	}

      }
    }
  }

  if (sumOfValidStates != 0) {
    amplitude normalize = amplitude(1) / std::sqrt(sumOfValidStates);
    /*std::cout << "MEASURED " << measure << std::endl;
    std::cout << "SUM OF INVALID (" << ind << "): " << sumOfInvalidStates << "(" << invCount << " invalid states)" << std::endl;
    std::cout << "Valid states: " << validStates.size() << std::endl;
    */
    for (long i : validStates) {
      amps[i] *= normalize;
    }
  }

  for(long i = 0; i < maxUsed; ++i) {
    totalProb += pow(std::abs(amps[i]), 2);
  }
  std::cout << "MEASURE " << measure << std::endl;
  std::cout << "TOTAL PROB: " << totalProb << std::endl;
  
  for(int i = 0; i < -1; ++i) {
  printf("HAI\n");
}

  std::cout << "STATES: " << startStates << " -> " << validStates.size() << std::endl;

  return measure;
}

regState quReg::measure_NO_COLLAPSE() {
  double rand = getRand();
  regState ans = 0;
  bool found = false;
  double accProb = 0;
  int losses = 0;
  
  for (long i = 0; i < ((long)1 << size); ++i) {break;
    if (getAmplitude(i) == amplitude(0)) continue;
    double prob = pow(std::abs(getAmplitude(i)), 2);
    if (rand - accProb > prob) {
      double oldAccProb = accProb;
      accProb += prob;

      std::cout << rand << " " << oldAccProb << " " << accProb << " " << prob << std::endl;
      getchar();
      
    }
    else {
      ans = i;
      found = true;
      break;
    }
  }
  
  for(long i = 0; i < maxUsed; ++i) {
    double prob = pow(std::abs(amps[i]), 2);
    if (rand - accProb > prob) {
      double oldAccProb = accProb;
      accProb += prob;
      
      if (oldAccProb == accProb) {
	//std::cout << "WARNING: precision loss" << std::endl;
	++losses;
      }
      
    }
    else {
      ans = states[i];
      found = true;
      break;
    }
  }
  
  
  

  if (!found) {
    std::cerr << "ERROR: could not measure register (" << accProb << " losses " << losses << "/" << maxUsed << ")" << std::endl;
  }

  return ans;
}

regState quReg::measure() {
  regState measure = measure_NO_COLLAPSE();

  clear();
  setAmplitude(measure, 1);
}

void quReg::TOFFOLI(int c1, int c2, int t) {
  //Eigen::MatrixXcd gate;
  // int c1_ = (size-1)-c1;
  // int c2_ = (size-1)-c2;
  // int t_ = (size-1)-t;

//   for (long i = 0; i < ((long)1 << (size-3)); ++i) {break;
//     // std::vector<int> pattern = getStateAsList(i, this->size-3);
//     regState patternR = i;
//  
//     int min = std::min(std::min(c1, c2), t);
//     int max = std::max(std::max(c1, c2), t);
//     int mid = c1^c2^t^min^max;
// 
//     // int min_ = std::min(std::min(c1_, c2_), t_);
//     // int max_ = std::max(std::max(c1_, c2_), t_);
//     // int mid_ = c1_^c2_^t_^min_^max_;
// 
//     shiftAtBit(patternR, min);
//     shiftAtBit(patternR, mid);
//     shiftAtBit(patternR, max);
//     // pattern.insert(pattern.begin() + min_, 0);
//     // pattern.insert(pattern.begin() + mid_, 0);
//     // pattern.insert(pattern.begin() + max_, 0);
//     
//     setBitAt(patternR, c1, 1);
//     // pattern[c1_] = 1;
//     setBitAt(patternR, c2, 1);
//     // pattern[c2_] = 1;
//     
//     setBitAt(patternR, t, 0);
//     // pattern[t_] = 0;
//     regState state110 = patternR;
//     
//     
//     setBitAt(patternR, t, 1);
//     // pattern[t_] = 1;
//     regState state111 = patternR;
//     
//     amplitude amplitude110 = getAmplitude(state110);
//     amplitude amplitude111 = getAmplitude(state111);
// 
//     setAmplitude(state110, amplitude111);
//     setAmplitude(state111, amplitude110);
// 
//   }

  for(long i = 0; i < maxUsed; ++i) {
    if (states[i] & ((long)1 << c1)) {
      if (states[i] & ((long)1 << c2)) {
      states[i] ^= ((long)1 << t);
      }
    }
  }
  
  
}

void quReg::addBitsToRegister(int bitsToAdd) {
  this->size += bitsToAdd;
 
//   states = (amplitude**) realloc(states, ((long)1 << size) * sizeof(amplitude*));
// 
//   if (states == nullptr) {
//     std::cerr << "ERROR: could not resize quReg. Exiting ..." << std::endl;
//     exit(0);
//   }
//   else {
//     std::cout << "Successful realloc" << std::endl;    
//   }

  for(long i = ((long)1 << (size-bitsToAdd)); i < ((long)1 << size); ++i) {
    //std::cout << i << std::endl;
//     states[i] = nullptr;
  }
  
  //std::cout << "Zero'd out the new memory" << std::endl;    
  
  for(long i = ((long)1 << (size-bitsToAdd))-1; i >= 0 ; --i) {
    if (getAmplitude(i) == amplitude(0)) continue;
    setAmplitude(i << bitsToAdd, getAmplitude(i));
    if (i != 0) setAmplitude(i, amplitude(0));
  }

  //std::cout << "New memory from " << std::bitset<64>((long)1 << (size-bitsToAdd))
  //<< " (" << ((long)1 << (size-bitsToAdd)) << ") to "
  //<< std::bitset<64>(((long)1 << size)) << " (" << ((long)1 << size) << ")" << std::endl;


  // std::vector<partialState> newStates;
  
  // for(auto ite = states.begin(); ite != states.end(); ++ite) {
  //   newStates.push_back(partialState(ite->first << bitsToAdd, ite->second));
  // }

  // states.clear();

  // for(int i = 0; i < newStates.size(); ++i) {
  //   states[newStates[i].first] = newStates[i].second;
  // }

}

void quReg::applyFunction(int bitsNum, std::function<int(long)> lambda) {
  for(long i = 0; i < 1 << (bitsNum); ++i) {
    regState inputState = i;

    for(long j = 0; j < (1<<(size-bitsNum)); ++j) {
      regState state = inputState + (j<< bitsNum);
      amplitude stateAmp = getAmplitude(state);
      setAmplitude(state, amplitude(1-2*lambda(i)) * stateAmp);
    }
  }

}

void quReg::CPHASE(double phase, int c, int t) {
  //Eigen::MatrixXcd gate;
  
  amplitude factor = std::exp(amplitude(0, phase));

  // std::cout << factor << " " << std::abs(factor) << std::endl;
  
  for(long i = 0; i < maxUsed; ++i) {
    if (states[i] & ((long)1 << c)) {
      if (states[i] & ((long)1 << t)) {
	amps[i] *= factor;
      }
    }
  }
  
}

void quReg::QFT(int width, int start) {
  for (int i = width-1+start; i >= start; --i) {
    
    for (int j = width-1+start; j > i; --j) {
      double phase = M_PI / ((long)1 << (j-i));
      CPHASE(phase, j, i);
    }
    HADAMARD(i);

  }
  
}

void quReg::removeBitsFromRegister(int bitsToRemove) {
  for(long i = 0; i < ((long)1 << size); ++i) {break;
    if (getAmplitude(i) != amplitude(0)) {
        setAmplitude(i >> bitsToRemove, getAmplitude(i));
	setAmplitude(i, 0);
    }
    
  }
  
  for(long i = 0; i < maxUsed; ++i) {
    if (amps[i] != amplitude(0)) {
      states[i] >>= bitsToRemove;
    }
    else {
      states[i] = -1;
    }
  }
  
  this->size -= bitsToRemove;
  
}

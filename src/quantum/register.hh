#ifndef __QUREG__HH__
#define __QUREG__HH__

#include <map>
#include <initializer_list>
#include <iostream>
#include <cstdlib>
#include <bitset>
#include <cmath>
#include <vector>
#include <complex>
#include <functional>

#include "../json/json.hh"

typedef long regState;
typedef std::complex<double> amplitude;
typedef std::pair<regState, amplitude> partialState;

class quReg {
private:
  int size; // number of quBits in the register
  //amplitude** states;
  //std::map<regState, amplitude*> states;

  //StatesManager states;
  long internalSize;
 
  Json json;
  
public:
  quReg(int size);
  ~quReg();

  long maxUsed;
  amplitude* amps;
  long* states;

  amplitude** getStates() {return nullptr;}
  int getSize() {return size;}
  long* getSt() {return states;};
  long getIndex(regState rState);

  long setAmplitude(partialState ps);
  long setAmplitude(regState rS, amplitude a) {return setAmplitude(partialState(rS, a));}
  amplitude getAmplitude(regState rS);
  void clear();

  void print(std::ostream& o);

  void NOT(int i);
  void HADAMARD(int i);
  void CNOT(int c, int t);
  void TOFFOLI(int c1, int c2, int t);
  void CPHASE(double phase, int c, int t);
  
  void QFT(int width, int start);

  int measureQuBit(int i);
  int measureQuBit_NO_COLLAPSE(int i);
  regState measure();
  regState measure_NO_COLLAPSE();

  void addBitsToRegister(int bitsToAdd);
  void removeBitsFromRegister(int bitsToRemove);

  void applyFunction(int bitsNum, std::function<int(long)> lambda);

  void record() {json.record(*this);}
  void record(std::string title) {json.record(*this, title);}
  void saveToFile(std::string fileName){json.writeToFile(fileName);}

};

std::ostream& operator<<(std::ostream& o, quReg& q);

static regState getState(std::vector<int> v) {
  regState ans = 0;
  int ind = v.size()-1;

  for (int i : v) {
    if (i > 0) {
      ans += (1 << ind);
    }

    --ind;
  }

  return ans;
}

static std::vector<int> getStateAsList(regState rS, int size) {
  std::vector<int> ans;
  
  for(int i = size-1; i >= 0; --i) {
    ans.push_back( (rS & (1 << i)) >> i);
  }

  return ans;
}

static double getRand() {
  return ((double)rand()) / RAND_MAX;
}

static void shiftAtBit(regState& pattern, int ind) {
  regState lowerMask = ((1 << ind) - 1);
  regState upperMask = ~lowerMask;

  regState result = ((pattern & upperMask) << 1) + (pattern & lowerMask);

  pattern = result;
}

static void setBitAt(regState& r, int ind, bool value) {
  if (value) {
    r = r | (1<<ind);
  }
  else {
    r = r & ~(1<<ind);
  }
}

#endif
